package store_warehouse.store.data.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import store_warehouse.store.data.enums.Color;
import store_warehouse.store.data.enums.Type;

import java.math.BigDecimal;

@Data
@Builder
public class Clothes {
    @JsonIgnore
    public static int count;

    private Integer id;
    private Type type;
    private Color color;
    private Integer size;
    private BigDecimal price;
    private String description;
}
