package store_warehouse.store.data.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Type {
    DRESS("Платье"),
    TROUSERS("Брюки"),
    SKIRT("Юбка"),
    VEST("Жилет"),
    SHIRT("Рубашка");
    private String name;

}
