package store_warehouse.store.data.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Color {
    WHITE("Белый"),
    BLUE("Синий"),
    RED("Красный"),
    GREEN("Зеленый"),
    BLACK("Черный");

    private String name;
}
