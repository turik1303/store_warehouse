package store_warehouse.store.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import store_warehouse.store.components.Location;
import store_warehouse.store.data.entities.Clothes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Service
public class ClothesService {
    private final Location location;

    @Autowired
    public ClothesService(Location location) {
        this.location = location;
    }

    public Clothes addToWarehouse(Clothes clothes) {
        // generating cloth id from static field
        clothes.setId(Clothes.count + 1);
        // increment clothes counter to generate ids
        Clothes.count++;
        location.getWarehouse().put(clothes.getId(), clothes);
        return clothes;
    }

    public List<Clothes> findAll() {
        List<Clothes> allClothes = new ArrayList<>();
        // getting all clothes from warehouse
        allClothes.addAll(location.getWarehouse().values());
        // getting all clothes from store
        allClothes.addAll(location.getStore().values());
        return allClothes;
    }

    public List<Clothes> findByLocation(String location) throws Exception {
        switch (location) {
            case "store": return new ArrayList<>(this.location.getStore().values());
            case "warehouse": return new ArrayList<>(this.location.getWarehouse().values());
            default: throw new Exception("Not found location \"" + location + "\"");
        }
    }

    public Clothes findById(Integer id) throws Exception {
        // collecting all clothes to stream and then searching for clothes by id
        Clothes clothes = Stream.of(location.getWarehouse().values(), location.getStore().values())
                .flatMap(Collection::stream)
                .filter(cloth -> cloth.getId().equals(id))
                .findFirst()
                .get();
        if (clothes != null) {
            return clothes;
        } else {
            throw new Exception("Clothes with id \"" + id.toString() + "\" not found");
        }
    }

    public void deleteById(Integer id) throws Exception {
        if (location.getStore().containsKey(id)) {
            location.getStore().remove(id);
        } else if (location.getWarehouse().containsKey(id)) {
            location.getWarehouse().remove(id);
        } else {
            throw new Exception("Clothes with id \"" + id.toString() + "\" not found");
        }
    }

    // TODO: fix update
    public void update(Clothes clothes) throws Exception {
        if (location.getStore().containsKey(clothes.getId())) {
            location.getStore().put(clothes.getId(), clothes);
        } else if (location.getWarehouse().containsKey(clothes.getId())) {
            location.getWarehouse().put(clothes.getId(), clothes);
        } else {
            throw new Exception("Clothes with id \"" + clothes.getId().toString() + "\" not found");
        }
    }

    public void move(String from, Integer id) throws Exception {
        Map<Integer, Clothes> source;
        Map<Integer, Clothes> destination;
        switch (from) {
            case "store": {
                source = location.getStore();
                destination = location.getWarehouse();
                break;
            }
            case "warehouse": {
                source = location.getWarehouse();
                destination = location.getStore();
                break;
            }
            default: throw new Exception("Source location \"" + from + "\" not found");
        }

        if (source.containsKey(id)) {
            Clothes movingCloth = source.get(id);
            source.remove(id);
            destination.put(id, movingCloth);
        } else {
            throw new Exception("Clothes with id = " + id.toString() + " not found in location \"" + source + "\"");
        }
    }
}
