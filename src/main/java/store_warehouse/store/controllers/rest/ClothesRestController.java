package store_warehouse.store.controllers.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import store_warehouse.store.data.entities.Clothes;
import store_warehouse.store.services.ClothesService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/clothes")
public class ClothesRestController {
    private final ClothesService clothesService;

    @Autowired
    public ClothesRestController(ClothesService clothesService) {
        this.clothesService = clothesService;
    }

    /*
    * route for getting all clothes from both store and warehouse
    * */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Clothes> findAll() {
        return clothesService.findAll();
    }

    /*
    * route for getting clothes by id
    * */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Clothes findById(@PathVariable String id) throws Exception {
        return clothesService.findById(Integer.parseInt(id));
    }

    /*
    * route for getting all clothes in location (store | warehouse)
    * */
    @RequestMapping(value = "/by-location/{location}", method = RequestMethod.GET)
    public List<Clothes> findByLocation(@PathVariable String location) throws Exception {
        return clothesService.findByLocation(location);
    }

    /*
    * route to create new clothes on warehouse
    * */
    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public Clothes create(@RequestBody Clothes clothes) {
        return clothesService.addToWarehouse(clothes);
    }

    /*
    *route for updating existing clothes
    * */
    @RequestMapping(value = "/", method = RequestMethod.PATCH)
    public void update(@RequestBody Clothes clothes) throws Exception {
        clothesService.update(clothes);
    }

    /*
    * route to move clothes from store to warehouse and from warehouse to store
    * */
    @RequestMapping(value = "/move", method = RequestMethod.POST)
    public void move(@RequestBody Map<String,Object> body) throws Exception {

        String from = (String) body.get("from");
        Integer id = (Integer) body.get("id");

        clothesService.move(from, id);
    }

    /*
    * route for deleting clothes by id
    * */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String id) throws Exception {
        clothesService.deleteById(Integer.parseInt(id));
    }

}
