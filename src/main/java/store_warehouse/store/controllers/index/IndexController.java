package store_warehouse.store.controllers.index;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping(value = "/aaa")
    public String onEnter() {
        return "index.html";
    }
}
