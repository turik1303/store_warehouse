package store_warehouse.store.components;

import lombok.Getter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import store_warehouse.store.data.entities.Clothes;

import java.util.HashMap;
import java.util.Map;

@Component
@Scope(value = "singleton")
@Getter
public class Location {

    private Map<Integer, Clothes> store;
    private Map<Integer, Clothes> warehouse;

    public Location() {
        store = new HashMap<>();
        warehouse = new HashMap<>();
    }



}
